#-------------------------------------------------
#
# Project created by QtCreator 2018-02-24T16:22:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#QT       -= core gui
TARGET = h264_play
#TEMPLATE = lib
TEMPLATE = app
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS __STDC_FORMAT_MACROS __STDC_CONSTANT_MACROS MEDIA_PLAYER_LIB_LIBRARY
QMAKE_CXXFLAGS += -std=c++0x -fpermissive



INCLUDEPATH += $$PWD/3rt/ffmpeg/include \
    $$PWD/3rt/nanomsg/include \
    $$PWD/3rt/protobuf/include \
    $$PWD/3rt/SDL2/include \
    $$PWD/3rt/tinyxml2/include

LIBS += $$PWD/3rt/nanomsg/lib/libnanomsg.dll.a
LIBS += $$PWD/3rt/ffmpeg/lib/avutil.lib
LIBS += $$PWD/3rt/ffmpeg/lib/avformat.lib
LIBS += $$PWD/3rt/ffmpeg/lib/avcodec.lib
LIBS += $$PWD/3rt/ffmpeg/lib/swscale.lib
LIBS += $$PWD/3rt/protobuf/lib/libprotobuf.a
LIBS += $$PWD/3rt/SDL2/lib/libSDL2.dll.a
LIBS += $$PWD/3rt/SDL2/lib/libSDL2_ttf.dll.a
LIBS += $$PWD/3rt/tinyxml2/lib/libtinyxml2.dll.a

#INCLUDEPATH += /usr/local/include

#LIBS += -L/usr/local/lib -L/usr/local/lib64 -lnanomsg -lavutil -lavformat -lavcodec -lswscale -lprotobuf -lSDL2

HEADERS += \
    play/h264_decoder.h \
    play/h264_play.h \
    play/timer_task.h \
    proto/media_server.pb.h \
    service/h264_key.h \
    service/nn_helper.h \
    service/nn_req.h \
    service/nn_sub.h \
    util/args_info.h \
    util/thread_safe_queue.h \
    util/util.h \
    ui/player_ui.h \
    ui/player_config_ui.h

SOURCES += \
    play/h264_decoder.cpp \
    play/h264_play.cpp \
    play/timer_task.cpp \
    proto/media_server.pb.cc \
    service/h264_key.cpp \
    service/nn_helper.cpp \
    service/nn_req.cpp \
    service/nn_sub.cpp \
    util/util.cpp \
    ui/player_ui.cpp \
    ui/player_config_ui.cpp

OTHER_FILES += \
    util/configure.xml

FORMS += \
    ui/player_ui.ui \
    ui/player_config_ui.ui


DISTFILES += \
          res.rc
RC_FILE = res.rc

