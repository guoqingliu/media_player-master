#ifndef PLAY_MODE_SWITCH_CONFIG_H
#define PLAY_MODE_SWITCH_CONFIG_H

#include <QWidget>
#include <QDate>

namespace Ui {
class play_mode_switch_config;
}

typedef struct
{
    int _lab;
    int _dev;
    int _mode;
    unsigned long long _begin_pts;
    unsigned long long _end_pts;
}play_args_t;

class play_mode_switch_config : public QWidget
{
    Q_OBJECT

public:
    explicit play_mode_switch_config(QWidget *parent, void(*post_handle)(void*));
    ~play_mode_switch_config();
    play_args_t* get_args()
    {
        if(_flag)
            return &_args;
        return 0;
    }
    void reset_flag()
    {
        _flag = 0;
    }

private slots:
    void on_cb_mode_currentIndexChanged(int index);

    void on_pb_ok_clicked();

    void on_pb_cancel_clicked();

private:
    Ui::play_mode_switch_config *ui;
    play_args_t _args;
    void(*_post_handle)(void*);
    int _flag;
};

#endif // PLAY_MODE_SWITCH_CONFIG_H
