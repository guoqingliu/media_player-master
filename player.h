#ifndef PLAYER_H
#define PLAYER_H

#include <QWidget>
#include "play_mode_switch_config.h"


namespace Ui {
class player;
}

class player : public QWidget
{
    Q_OBJECT

public:
    explicit player(QWidget *parent = 0);
    ~player();
    void config_post_handle_impl();
    void progress_handle_impl(unsigned long long pts);
private slots:
    void on_pb_seek_valueChanged(int value);
    void on_pb_switch_clicked();
    void on_pb_play_clicked();

    void on_pb_pause_clicked();

private:
    void closeEvent(QCloseEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void init_chn_control();
    void init_win();
private:
    Ui::player *ui;
    void* _play;
    play_mode_switch_config* _config_win;
};

#endif // PLAYER_H
