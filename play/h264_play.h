//
// Created by rio on 3/20/18.
//

#ifndef MEDIA_SERVER_H264_PLAY_H
#define MEDIA_SERVER_H264_PLAY_H

#if defined(MEDIA_PLAYER_LIB_LIBRARY)
#  define MEDIA_PLAYER_LIBSHARED_EXPORT extern "C" __declspec(dllexport)
#else
#  define MEDIA_PLAYER_LIBSHARED_EXPORT extern "C" __declspec(dllimport)
#endif

extern "C"
{
typedef struct
{
    void* _win_id;
    int _sws_width;
    int _sws_height;
}play_win_info_t;
}

MEDIA_PLAYER_LIBSHARED_EXPORT
void timer_task_loop();

MEDIA_PLAYER_LIBSHARED_EXPORT
void timer_task_break_loop();

MEDIA_PLAYER_LIBSHARED_EXPORT
void* h264_player_init(play_win_info_t* args, size_t size);

MEDIA_PLAYER_LIBSHARED_EXPORT
int h264_player_play_or_pause(void* opaque);

MEDIA_PLAYER_LIBSHARED_EXPORT
void h264_player_set_args(void* opaque, int lab, int dev, int mode, unsigned long long begin_pts, unsigned long long end_pts);

MEDIA_PLAYER_LIBSHARED_EXPORT
int h264_player_check_args(void* opaque, int lab, int dev, int mode, unsigned long long begin_pts, unsigned long long end_pts);

MEDIA_PLAYER_LIBSHARED_EXPORT
void h264_player_get_args(void* opaque, int* lab, int* dev, int* mode, unsigned long long* begin_pts, unsigned long long* end_pts);

MEDIA_PLAYER_LIBSHARED_EXPORT
int h264_player_seek(void* opaque, int percent);

MEDIA_PLAYER_LIBSHARED_EXPORT
int h264_player_pts_to_percent(void* opaque, unsigned long long pts);

MEDIA_PLAYER_LIBSHARED_EXPORT
void h264_player_teardown(void* opaque);

MEDIA_PLAYER_LIBSHARED_EXPORT
void h264_player_switch_mode(void* opaque, int lab, int dev, int mode, unsigned long long begin_pts, unsigned long long end_pts);

MEDIA_PLAYER_LIBSHARED_EXPORT
void h264_player_set_progress_handle(void* opaque, void(*handle)(unsigned long long));

MEDIA_PLAYER_LIBSHARED_EXPORT
const char* args_info_labs();

MEDIA_PLAYER_LIBSHARED_EXPORT
const char* args_info_devs();

MEDIA_PLAYER_LIBSHARED_EXPORT
const char* args_info_chn_range(int chn);

MEDIA_PLAYER_LIBSHARED_EXPORT
time_t mk_time(int y, int mon, int d, int h, int min, int s);

MEDIA_PLAYER_LIBSHARED_EXPORT
unsigned long long gen_default_pts(const time_t *time);

#include <SDL2/SDL.h>
#include "../service/nn_req.h"
#include "../service/nn_sub.h"
#include "h264_decoder.h"

#define PS_PAUSE    0X01
#define PS_SEEK     0X02
#define PS_TERM     0X04

class basic_play
{
public:
    virtual int init(const void* win_id, int width, int height, const char* url) = 0;

    virtual void term() = 0;

    virtual int setup(char lab, char dev, char chn,
                      uint64_t begin_pts = 0, uint64_t end_pts = -1) = 0;
    int get_status(){return _status;}
    void seek(uint64_t pts)
    {
        std::lock_guard<std::mutex> lock{_mtx};
        _status |= PS_SEEK;
        _seek_pts = pts;
    }

    void play_or_pause()
    {
        std::lock_guard<std::mutex> lock{_mtx};
        if(_status & PS_PAUSE)
            _status &= ~PS_PAUSE;
        else
            _status |= PS_PAUSE;
    }

    uint64_t cur_pts()
    {
        return _pts;
    }

protected:
    int basic_init(const void* win_id, int width, int height)
    {
        SDL_Init(SDL_INIT_VIDEO);
        _win = 0;
        _render = 0;
        _texture = 0;
        _decoder = 0;
        _pts = AV_NOPTS_VALUE;
        _seek_pts = AV_NOPTS_VALUE;
        _status = 0;
        _wrk_thr = 0;

        _decoder = new h264_decoder(width, height);
        _rect = {0, 0, width, height};

        if((_win = SDL_CreateWindowFrom(win_id)) == 0)
            goto failed;

        if((_render = SDL_CreateRenderer(_win, -1, 0)) == 0)
            goto failed;

        if((_texture = SDL_CreateTexture(_render, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING,
                                         width, height)) == 0)
            goto failed;
        return 0;

    failed:
        if(_decoder)
            delete _decoder;
        if(_texture)
            SDL_DestroyTexture(_texture);

        if(_render)
            SDL_DestroyRenderer(_render);

        if(_win)
            SDL_DestroyWindow(_win);
        return -1;
    }

    void basic_term()
    {
        {
            std::lock_guard<std::mutex> lock{_mtx};
            _status |= PS_TERM;
        }

        if(_wrk_thr)
            _wrk_thr->join();

        if(_texture)
            SDL_DestroyTexture(_texture);

        if(_render)
            SDL_DestroyRenderer(_render);

        if(_win)
            SDL_DestroyWindow(_win);

        delete _wrk_thr;
        delete _decoder;
    }

    int on_frame(const void *data, size_t data_size)
    {
        return _decoder->decode(data, data_size, [&](AVFrame* frame){

            SDL_UpdateYUVTexture(_texture, 0,
                                 frame->data[0], frame->linesize[0],
                    frame->data[1], frame->linesize[1],
                    frame->data[2], frame->linesize[2]);
            SDL_RenderCopy(_render, _texture, 0, &_rect);
            SDL_RenderPresent(_render);
        });
    }

protected:
    virtual int seek_impl(uint64_t pts) = 0;

    virtual int play_impl() = 0;

    void loop()
    {
        while(true)
        {
            std::lock_guard<std::mutex> lock{_mtx};

            if(_status & PS_TERM)
            {
                return;
            }

            if(_status & PS_SEEK)
            {
                seek_impl(_seek_pts);
                _status &= ~PS_SEEK;
            }

            if(_status & PS_PAUSE)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(20));
                continue;

            }
            if(!(_status & PS_PAUSE))
                play_impl();
        }
    }

    void set_pts(uint64_t pts)
    {
        if(pts != AV_NOPTS_VALUE)
            _pts = pts;
    }

protected:
    int _status;
    uint64_t _pts;
    uint64_t _seek_pts;
    std::mutex _mtx;
    std::thread* _wrk_thr;
private:
    SDL_Window* _win;
    SDL_Renderer* _render;
    SDL_Texture* _texture;
    SDL_Rect _rect;
    h264_decoder* _decoder;
};

class req_play : public basic_play
{
public:
    int init(const void *win_id, int width, int height,
             const char* rep_url) override
    {
        _req = 0;
        _id = -1;

        if(basic_init(win_id, width, height) < 0)
            goto failed;

        _req = new nn_req(rep_url);
        return 0;
    failed:
        basic_term();
        delete _req;
        return -1;
    }

    void term() override
    {
        basic_term();
        _req->get_video_teardown(_id);
        delete _req;
    }

    int setup(char lab, char dev, char chn,
              uint64_t begin_pts, uint64_t end_pts) override
    {
        if(_req->get_video_setup(&_id, lab, dev, chn, begin_pts, end_pts) < 0)
            return -1;

        _wrk_thr = new std::thread{[&](){
            loop();
        }};

        return 0;
    }

    int seek_impl(uint64_t pts) override
    {
        if(_req->get_video_seek(_id, pts) < 0)
            return -1;

        set_pts(pts);

        return 0;
    }

    int play_impl() override
    {
        uint8_t* data;
        size_t data_size;
        uint64_t pts;

        if(_req->get_video_play(_id, &data, &data_size, &pts) < 0)
            return -1;

        set_pts(pts);

        if(on_frame(data, data_size) < 0)
            return -1;
        return 0;
    }
private:
    uint64_t _id;
    nn_req* _req;
};

class sub_play : public basic_play
{
public:
    int init(const void *win_id, int width, int height,
             const char* pub_url) override
    {
        _sub = 0;

        if(basic_init(win_id, width, height) < 0)
            goto failed;

        _sub = new nn_sub(pub_url);
        return 0;
    failed:
        basic_term();
        delete _sub;
        return -1;
    }

    void term() override
    {
        basic_term();
        delete _sub;
    }

    int setup(char lab, char dev, char chn,
              uint64_t begin_pts, uint64_t end_pts) override
    {
        (void)begin_pts;
        (void)end_pts;

        char topic[0x20];
        size_t topic_size;

        h264_key key{lab, dev, chn};
        topic_size = key.serialize_to_arrary(topic, sizeof(topic));

        if(_sub->sub(topic, topic_size) < 0)
            return -1;
        _wrk_thr = new std::thread{[&](){
            loop();
        }};
        return 0;
    }

    int seek_impl(uint64_t pts)
    {
        return 0; //ignore
    }

    int play_impl()
    {
        return _sub->step([&](h264_key key, const uint8_t* data, size_t data_size, uint64_t pts){
            (void)key;
            set_pts(pts);           
            if(on_frame(data, data_size) < 0)
                return -1;
            return 0;
         });
    }

private:
    nn_sub* _sub;
};

#endif //MEDIA_SERVER_H264_PLAY_H
