//
// Created by rio on 3/8/18.
//

#ifndef MEDIA_SERVER_TIMER_TASK_H
#define MEDIA_SERVER_TIMER_TASK_H
#include <map>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <cstdint>
#include <sys/time.h>
#include <thread>
#include "../util/util.h"

#define TT_MGR_PTS 0x0
#define TT_INIT_PTS 0x1000

class timer_task
{
    typedef void(*task_callback_t)(void*);
    typedef std::tuple<void*, task_callback_t, const char*> task_t;
    typedef std::multimap<uint64_t, task_t> task_map_t;
public:
    timer_task();
    ~timer_task();
    static timer_task* get();
    static timer_task* get_main_loop();
    void add_task(uint64_t us, void* opaque, task_callback_t callback, const char* des = 0);
    void step();
    void loop();
    void break_loop();
private:
    int _break_loop;
    task_map_t _tasks;
    std::mutex _mtx;
    std::condition_variable _cond;
};
#endif //MEDIA_SERVER_TIMER_TASK_H
