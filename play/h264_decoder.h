//
// Created by rio on 3/8/18.
//

#ifndef H264_DECODER_H
#define H264_DECODER_H

#include <thread>
#include "../util/util.h"

class h264_decoder
{
public:
    h264_decoder(int sws_width, int sws_height);
    ~h264_decoder();

    //Note
    void setWH(int width,int height)
    {
        _sws_width = width;
        _sws_height = height;
    }

    int init_sws(int width, int height);
    void resize(int width, int height)
    {
       _sws_width = width;
       _sws_height = height;
       if(_sws_ctx)
       {
           sws_freeContext(_sws_ctx);
           _sws_ctx = 0;
       }
    }
    template<typename _Frame_Callback>
    int decode(const void* data, size_t data_size, _Frame_Callback frame_callback)
    {
        int ret;
        //print_buf((uint8_t*)data, data_size);
        while(data_size)
        {
            if((ret = av_parser_parse2(
                    _codec_parser_ctx, _codec_ctx,
                    &_pkt->data, &_pkt->size,
                    (const uint8_t*)data , data_size ,
                    AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0)) < 0)
                return -1;
            data = (const uint8_t*)data + ret;
            data_size -= ret;
            if(_pkt->size > 0 && decode(frame_callback) < 0)
                return -1;
        }
        return 0;
    }

    template<typename _Frame_Callback>
    int decode(_Frame_Callback callback)
    {
        int ret;

        if((ret = avcodec_send_packet(_codec_ctx, _pkt)) < 0)
        {
            log_err("avcodec_send_packet", ret);
            return -1;
        }
        while(ret >= 0)
        {
            ret = avcodec_receive_frame(_codec_ctx, _frame);
            if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
                return 0;
            else if (ret < 0)
            {
                log_err("avcodec_receive_frame", ret);
                return -1;
            }

            if(_sws_ctx == 0)
                init_sws(_frame->width, _frame->height);

            sws_scale(_sws_ctx, (uint8_t const * const *)_frame->data,
                      _frame->linesize, 0, _frame->height,
                      _sws_frame->data, _sws_frame->linesize);

            callback(_sws_frame);
        }
        return 0;
    }

private:
    void log(const char *what, const char *fmt, ...);
    void log_err(const char *what, int err);

private:
    int _sws_width;
    int _sws_height;
    int _width;
    int _height;
    SwsContext* _sws_ctx;
    AVPacket* _pkt;
    AVFrame* _frame;
    AVFrame* _sws_frame;
    AVCodec* _codec;
    AVCodecParserContext* _codec_parser_ctx;
    AVCodecContext* _codec_ctx;
};



#endif //H264_DECODER_H
