//
// Created by rio on 3/8/18.
//

#include "h264_decoder.h"

h264_decoder::h264_decoder(int sws_width, int sws_height) :
        _sws_width(sws_width), _sws_height(sws_height), _sws_ctx(0)
{
    avcodec_register_all();
    if((_codec = avcodec_find_decoder(AV_CODEC_ID_H264)) == 0)
        throw;
    if((_codec_ctx = avcodec_alloc_context3(_codec)) == 0)
        throw;
    if((_codec_parser_ctx = av_parser_init(AV_CODEC_ID_H264)) == 0)
        throw;
    if(avcodec_open2(_codec_ctx, _codec, 0) < 0)
        throw;
    if((_frame = av_frame_alloc()) == 0)
        throw;
    if((_pkt = av_packet_alloc()) == 0)
        throw;
    av_init_packet(_pkt);
    _sws_frame = 0;
}

h264_decoder::~h264_decoder() {

    if(_codec_parser_ctx)
        av_parser_close(_codec_parser_ctx);
    if(_codec_ctx) {
        avcodec_close(_codec_ctx);
        av_free(_codec_ctx);
    }
    if(_frame)
        av_frame_free(&_frame);
    if(_sws_frame)
        av_frame_free(&_sws_frame);
    if(_pkt)
        av_packet_free(&_pkt);
    if(_sws_ctx)
        sws_freeContext(_sws_ctx);

}

int h264_decoder::init_sws(int width, int height) {
    int num_bytes;
    uint8_t* buf;

    //printf("init_sws:%d,%d to %d,%d\n", width, height, _sws_width, _sws_height);

    if((_sws_ctx = sws_getContext(width, height, _codec_ctx->pix_fmt,
                                  _sws_width, _sws_height, AV_PIX_FMT_YUV420P,  SWS_BILINEAR,
                                  NULL, NULL, NULL)) == 0)
        goto failed;
    if((num_bytes = av_image_get_buffer_size(AV_PIX_FMT_YUV420P, _sws_width,
                                             _sws_height, 1)) < 0)
        goto failed;

    if((_sws_frame = av_frame_alloc()) == 0)
        goto failed;

    if((buf = (uint8_t *)av_malloc(num_bytes * sizeof(uint8_t))) == 0)
        goto failed;
    // required, or bad dst image pointers
    avpicture_fill((AVPicture *)_sws_frame, buf, AV_PIX_FMT_YUV420P,
                   _sws_width, _sws_height);
    return 0;
    failed:
    if(_sws_ctx)
    {
        sws_freeContext(_sws_ctx);
        _sws_ctx = 0;
    }
    if(_sws_frame)
        av_frame_free(&_sws_frame);

    return -1;
}

void h264_decoder::log(const char *what, const char *fmt, ...) {
    va_list va;
    va_start(va, fmt);
    default_vlog("h264_decoder", what, fmt, va);
    va_end(va);
}

void h264_decoder::log_err(const char *what, int err) {
    char err_buf[AV_ERROR_MAX_STRING_SIZE];
    av_make_error_string(err_buf, sizeof(err_buf), err);
    default_log("h264_decoder", what, err_buf);
}
