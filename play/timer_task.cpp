//
// Created by rio on 3/8/18.
//

#include "timer_task.h"

timer_task::timer_task() : _break_loop(0)
{}

timer_task::~timer_task() {}

timer_task *timer_task::get() {
    static timer_task task;
    return &task;
}

timer_task *timer_task::get_main_loop() {
    static timer_task task;
    return &task;
}

void timer_task::add_task(uint64_t us, void *opaque, timer_task::task_callback_t callback, const char* des) {
    std::lock_guard<std::mutex> lock{_mtx};
    _tasks.emplace(us, std::make_tuple(opaque, callback, des));
    _cond.notify_one();
}

void timer_task::step() {
    std::vector<task_t> timeup_tasks;
    uint64_t now_us, next_us;
    task_map_t::iterator it;

    next_us = -1;
    now_us = get_now_us();
    {
        std::lock_guard<std::mutex> lock{_mtx};
        for(it = _tasks.begin(); it != _tasks.end();)
        {
            if(it->first <= now_us)
            {
                timeup_tasks.push_back(it->second);
                it = _tasks.erase(it);
            } else
                break;
        }
        if(it != _tasks.end())
            next_us = it->first;
    }


    for(std::vector<task_t>::iterator timeup_task_it = timeup_tasks.begin(); timeup_task_it != timeup_tasks.end(); ++timeup_task_it)
    {
        (std::get<1>(*timeup_task_it))(std::get<0>(*timeup_task_it));
    }
/*
        now_us = get_now_us();
        if(next_us == -1)
            next_us = now_us + 10000;
        if(now_us < next_us)
        {
            std::unique_lock<std::mutex> lock{_mtx};
            _cond.wait_for(lock, std::chrono::microseconds{next_us - now_us});
        }
        */
    //std::this_thread::sleep_for(std::chrono::microseconds(100));
}

void timer_task::loop() {
    while(!_break_loop)
        step();
}

void timer_task::break_loop() {
    _break_loop = 1;
}
