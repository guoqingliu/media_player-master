//
// Created by rio on 3/20/18.
//

#include <vector>
#include <SDL2/SDL.h>
#include "timer_task.h"
#include "h264_decoder.h"
#include "../service/nn_sub.h"
#include "../service/nn_req.h"
#include "h264_play.h"
class basic_render;
class basic_play_task;
class req_task;
class sub_task;

enum play_status
{
    _init,
    _play,
    _pause,
    _teardown
};
class basic_paly_task
{
    friend req_task;
    friend sub_task;
public:
    basic_paly_task(const std::vector<basic_render*>& renderes, int lab, int dev);
    static void setup(void* opaque);
    static void play(void* opaque);
    static void pause(void* opaque);
    static void seek(void* opaque);
    static void teardown(void* opaque);
protected:
    virtual void setup_impl() = 0;
    virtual void play_impl() = 0;
    virtual void pause_impl() = 0;
    virtual void seek_impl() = 0;
    virtual void teardown_impl() = 0;
private:
    int _lab;
    int _dev;
    std::vector<basic_render*> _renderes;
    play_status _status;
};

class basic_render
{
public:
    basic_render(const play_win_info_t* args);
    ~basic_render();
    int render_nalu(const void* data, size_t data_size);
public:
    SDL_Window* _win;
    SDL_Rect _rect;
    SDL_Renderer* _render;
    SDL_Texture* _texture;
    h264_decoder* _decoder;
};

class req_task : public basic_paly_task
{
public:
    req_task(const std::vector<basic_render*>& renderes, const args_info* args,
             void(*progress_handle)(uint64_t), int lab, int dev,
             uint64_t begin_pts = 0, uint64_t end_pts = -1);
    ~req_task();

    void set_seek_pts(uint64_t pts);
protected:
    void setup_impl() override;
    void play_impl() override;
    void pause_impl() override;
    void seek_impl() override;
    void teardown_impl() override;

private:
    nn_req _req;
    uint64_t _begin_pts;
    uint64_t _end_pts;
    std::vector<uint64_t> _id_set;
    std::vector<uint64_t> _next_pts_set;
    uint64_t _seek_pts;
    void(*_progress_handle)(uint64_t);
};

class sub_task : public basic_paly_task
{
public:
    sub_task(const std::vector<basic_render*>& renderes, const args_info* args, int lab, int dev);
    ~sub_task();
protected:
    void setup_impl() override;
    void play_impl() override;
    void pause_impl() override;
    void seek_impl() override;
    void teardown_impl() override;

private:
    nn_sub _sub;
    std::vector<uint8_t> _id_check_set;
};

class render_play_task
{
public:
    render_play_task(std::vector<basic_render*>* renderes, size_t interval_us = 10000);
    static void step(void* opaque);
    static void break_loop(void* opaque);
private:
    void step_impl();
private:
    size_t _break_loop;
    size_t _next_us;
    size_t _interval_us;
    std::vector<basic_render*>* _renderes;
};

class h264_play
{
public:
    int init(const play_win_info_t* args, size_t size);
    int play_or_pause();
    void set_args(int lab, int dev, int mode, uint64_t begin_pts, uint64_t end_pts);
    int check_args(int lab, int dev, int mode, uint64_t begin_pts, uint64_t end_pts);
    void get_args(int* lab, int* dev, int* mode, uint64_t* begin_pts, uint64_t* end_pts);
    int seek(int percent);
    int pts_to_percent(uint64_t pts);
    void teardown();
    void switch_mode(int lab, int dev, int mode, uint64_t begin_pts, uint64_t end_pts);
    void set_progress_handle(void(*handle)(uint64_t));
private:
    //play args
    int _lab;
    int _dev;
    int _mode;
    uint64_t _begin_pts;
    uint64_t _end_pts;
    //req progress handle
    void(*_req_pg_handle)(uint64_t);
    std::vector<basic_render*> _renderes;
    req_task* _req_play;
    sub_task* _sub_play;
    render_play_task* _render_task;
};
render_play_task::render_play_task(std::vector<basic_render*>* renderes, size_t interval_us) :
        _renderes(renderes), _interval_us(interval_us), _next_us(get_now_us()), _break_loop(0)
{}

void render_play_task::step(void *opaque) {
    render_play_task* task = (render_play_task*)opaque;
    task->step_impl();
    task->_next_us += task->_interval_us;
    if(!task->_break_loop)
        timer_task::get()->add_task(task->_next_us, opaque, &render_play_task::step,
                                    "render_step");
}

void render_play_task::break_loop(void *opaque) {
    render_play_task* task = (render_play_task*)opaque;
    task->_break_loop = 1;
}

void render_play_task::step_impl() {
    SDL_Event event;
    SDL_PollEvent(&event);
    if(event.type == SDL_QUIT)
    {
        timer_task::get()->break_loop();
        return;
    }
    for(basic_render* render : *_renderes)
        SDL_RenderPresent(render->_render);
}


sub_task::sub_task(const std::vector<basic_render *> &renderes, const args_info *args, int lab, int dev) :
    basic_paly_task(renderes, lab, dev), _sub(args)
{
    setup_impl();
}

sub_task::~sub_task()
{
    int chn;
    char topic[0x20];
    size_t topic_size;

    for(chn = 0; chn < _id_check_set.size(); ++chn)
    {
        if(_id_check_set[chn])
        {
            h264_key key{_lab, _dev, chn};
            topic_size = key.serialize_to_arrary(topic, sizeof(topic));
            _sub.unsub(topic, topic_size);
        }
    }
    _id_check_set.clear();
}

void sub_task::setup_impl()
{
    int chn;
    char topic[0x20];
    size_t topic_size;

    for(chn = 0; chn < 0x04; ++chn)
    {
        h264_key key{_lab, _dev, chn};
        topic_size = key.serialize_to_arrary(topic, sizeof(topic));
        _sub.sub(topic, topic_size) < 0 ?
                    _id_check_set.push_back(0) : _id_check_set.push_back(1);
    }
    _status = play_status::_play;
    timer_task::get()->add_task(TT_INIT_PTS, this, &sub_task::play, "sub_play");
}

void sub_task::play_impl()
{
    int chn;
    size_t now_pts;

    now_pts = get_now_us();

    switch(_status)
    {
    case play_status::_play:
        for(chn = 0; chn < _id_check_set.size(); ++chn)
        {
            if(_id_check_set[chn])
            {
                _sub.step([&](h264_key key, const void* data, size_t data_size, uint64_t pts){
                    int index;

                    key.parse_cache_key(0, 0, &index);
                    _renderes[index]->render_nalu(data, data_size);
                    return 0;
                });
            }
        }
        timer_task::get()->add_task(TT_INIT_PTS, this, &sub_task::play, "sub_play");
        break;
    case play_status::_pause:
    {
        timer_task::get()->add_task(now_pts + TIME_US_BASE, this, &sub_task::play, "sub_pause");
    }
        break;
    case play_status::_teardown:
    {
        delete this;
    }
        break;
    }
}

void sub_task::pause_impl()
{
    _status = _status == play_status::_play ? play_status::_pause : play_status::_play;
}

void sub_task::seek_impl()
{
    throw;
}

void sub_task::teardown_impl()
{
    _status = play_status::_teardown;
}


req_task::req_task(const std::vector<basic_render *> &renderes, const args_info *args, void (*progress_handle)(uint64_t), int lab, int dev, uint64_t begin_pts, uint64_t end_pts) :
    basic_paly_task(renderes, lab, dev), _req(args),
    _begin_pts(begin_pts), _end_pts(end_pts), _progress_handle(progress_handle)
{
    setup_impl();
}

req_task::~req_task()
{
    int chn;
    uint64_t id;
    for(chn = 0; chn < _id_set.size(); ++chn)
    {
        if((id = _id_set[chn]) == -1)
            continue;
        _req.get_video_teardown(id);
    }
    _id_set.clear();
    _next_pts_set.clear();
}

void req_task::set_seek_pts(uint64_t pts)
{
    _seek_pts = pts;
}

void req_task::setup_impl()
{
    int chn;
    uint64_t id;

    _id_set.clear();
    _next_pts_set.clear();

    for(chn = 0; chn < 0x04; ++chn)
    {
        if(_req.get_video_setup(&id, _lab, _dev, chn, _begin_pts, _end_pts) < 0)
        {
            _id_set.push_back(-1);
        }
        else
        {
            _id_set.push_back(id);
        }
        _next_pts_set.push_back(AV_NOPTS_VALUE);
    }
    _status = play_status::_play;
    timer_task::get()->add_task(TT_INIT_PTS, this, &req_task::play, "req_play");
}

void req_task::play_impl()
{
    int chn;
    uint64_t id;
    uint64_t pts;
    void* data;
    size_t data_size;
    uint64_t now_pts;
    uint64_t min_pts;

    now_pts = get_now_us();
    min_pts = -1;

    switch(_status)
    {
    case play_status::_play:
        for(chn = 0; chn < _id_set.size(); ++chn)
        {
            if((id = _id_set[chn]) == -1)
                continue;
            if(_req.get_video_play(id, &data, &data_size, &pts) < 0)
            {
                _req.get_video_teardown(id);
                _id_set[chn] = -1;
                continue;
            }
            if(pts != AV_NOPTS_VALUE)
                min_pts = std::min(min_pts, pts);
            if(_renderes[chn]->render_nalu(data, data_size) < 0)
            {
                _req.get_video_teardown(id);
                _id_set[chn] = -1;
                continue;
            }
            _next_pts_set[chn] = (now_pts + TIME_US_BASE / 25.5);
        }
        if(min_pts != -1)
        {
            _progress_handle(min_pts);
        }
        timer_task::get()->add_task(now_pts + TIME_US_BASE / 25.5, this, &req_task::play, "req_play");
        break;
    case play_status::_pause:
    {
        timer_task::get()->add_task(now_pts + TIME_US_BASE, this, &req_task::play, "req_pause");
    }
        break;
    case play_status::_teardown:
    {
        delete this;
    }
        break;
    }
}

void req_task::pause_impl()
{
    _status = _status == play_status::_play ? play_status::_pause : play_status::_play;
}

void req_task::seek_impl()
{
    int chn;
    uint64_t id;
    for(chn = 0; chn < _id_set.size(); ++chn)
    {
        if((id = _id_set[chn]) == -1)
            continue;
        if(_req.get_video_seek(id, _seek_pts) < 0)
        {
            _req.get_video_teardown(id);
            _id_set[id] = -1;
        }
    }
}

void req_task::teardown_impl()
{
    _status = play_status::_teardown;
}


basic_render::basic_render(const play_win_info_t* args)
{
    _decoder = new h264_decoder(args->_sws_width, args->_sws_height);
    _rect = {0, 0, args->_sws_width, args->_sws_height};

    if((_win = SDL_CreateWindowFrom(args->_win_id)) == 0)
        goto failed;

    if((_render = SDL_CreateRenderer(_win, -1, 0)) == 0)
        goto failed;

    if((_texture = SDL_CreateTexture(_render, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING,
                                     args->_sws_width, args->_sws_height)) == 0)
        goto failed;
    return;

failed:
    if(_texture)
        SDL_DestroyTexture(_texture);

    if(_render)
        SDL_DestroyRenderer(_render);

    if(_win)
        SDL_DestroyWindow(_win);

    throw;
}

basic_render::~basic_render()
{
    if(_decoder)
        delete _decoder;

    if(_texture)
        SDL_DestroyTexture(_texture);

    if(_render)
        SDL_DestroyRenderer(_render);

    if(_win)
        SDL_DestroyWindow(_win);
}

int basic_render::render_nalu(const void *data, size_t data_size)
{
    return _decoder->decode(data, data_size, [&](AVFrame* frame){

        SDL_UpdateYUVTexture(_texture, 0,
                             frame->data[0], frame->linesize[0],
                frame->data[1], frame->linesize[1],
                frame->data[2], frame->linesize[2]);
        SDL_RenderCopy(_render, _texture, 0, &_rect);
    });
}


basic_paly_task::basic_paly_task(const std::vector<basic_render *> &renderes, int lab, int dev)
{
    _lab = lab;
    _dev = dev;
    _renderes = renderes;
    _status = play_status::_init;
}

void basic_paly_task::setup(void *opaque)
{
    ((basic_paly_task*)opaque)->setup_impl();
}

void basic_paly_task::play(void *opaque)
{
    ((basic_paly_task*)opaque)->play_impl();
}

void basic_paly_task::pause(void *opaque)
{
    ((basic_paly_task*)opaque)->pause_impl();
}

void basic_paly_task::seek(void *opaque)
{
    ((basic_paly_task*)opaque)->seek_impl();
}

void basic_paly_task::teardown(void *opaque)
{
    ((basic_paly_task*)opaque)->teardown_impl();
}


int h264_play::init(const play_win_info_t* args, size_t size)
{
    SDL_Init(SDL_INIT_VIDEO);
    if(size < 4)
        return -1;
    //width = range_list.at(0)
    for(int i = 0; i < size; ++i)
        _renderes.push_back(new basic_render(args + i));
    _render_task = new render_play_task(&_renderes);
    timer_task::get()->add_task(TT_INIT_PTS, _render_task, &render_play_task::step, "render_step");
    _req_play = 0;
    _sub_play = 0;
    _mode = -1;
    _req_pg_handle = 0;
    switch_mode(0, 0, 0, 0, -1);
}

int h264_play::play_or_pause()
{
    switch (_mode) {
    case 0:
        if(_sub_play)
            timer_task::get()->add_task(TT_MGR_PTS, _sub_play, &sub_task::pause, "sub_pause");
        break;
    case 1:
        if(_req_play)
            timer_task::get()->add_task(TT_MGR_PTS, _req_play, &req_task::pause, "req_pause");
        break;
    }
}

void h264_play::set_args(int lab, int dev, int mode, uint64_t begin_pts, uint64_t end_pts)
{
    _lab = lab;
    _dev = dev;
    _mode = mode;
    _begin_pts = begin_pts;
    _end_pts = end_pts;
}

int h264_play::check_args(int lab, int dev, int mode, uint64_t begin_pts, uint64_t end_pts)
{
    if(_lab == lab && _dev == dev && _mode == mode)
    {
        if(_mode == 0)
            return 0;
        else
        {
            if(_begin_pts == begin_pts && _end_pts == end_pts)
                return 0;
        }
    }
    return -1;
}

void h264_play::get_args(int *lab, int *dev, int *mode, uint64_t *begin_pts, uint64_t *end_pts)
{
    *lab = _lab;
    *dev = _dev;
    *mode = _mode;
    *begin_pts = _begin_pts;
    *end_pts = _end_pts;
}

int h264_play::seek(int percent)
{
    uint64_t pts;

    if(_mode != 1 || _req_play == 0)
    {
        return -1;
    }

    pts = _begin_pts + (_end_pts - _begin_pts) * percent / 100;
    _req_play->set_seek_pts(pts);
    timer_task::get()->add_task(TT_MGR_PTS, _req_play, &req_task::seek, "req_seek");
}

int h264_play::pts_to_percent(uint64_t pts)
{
    return (pts - _begin_pts) * 100 / (_end_pts - _begin_pts);
}

void h264_play::teardown()
{

    if(_req_play)
    {
        //timer_task::get()->add_task(TT_MGR_PTS, _req_play, &req_task::teardown);
        _req_play->teardown(_req_play);
        _req_play = 0;
    }

    if(_sub_play)
    {
        //timer_task::get()->add_task(TT_MGR_PTS, _sub_play, &sub_task::teardown);
        _sub_play->teardown(_sub_play);
        _sub_play = 0;
    }
}

void h264_play::switch_mode(int lab, int dev, int mode, uint64_t begin_pts, uint64_t end_pts)
{
    if(check_args(lab, dev, mode, begin_pts, end_pts) == 0)
        return;
    set_args(lab, dev, mode, begin_pts, end_pts);

    teardown();

    switch (mode) {
    case 0:
        _sub_play = new sub_task(_renderes, args_info::get(),
                                 lab, dev);
        // timer_task::get()->add_task(TT_MGR_PTS, _sub_play, &sub_task::play);
        break;
    case 1:
        _req_play = new req_task(_renderes, args_info::get(),
                                 _req_pg_handle,
                                 lab, dev,
                                 begin_pts, end_pts);
        //timer_task::get()->add_task(TT_MGR_PTS, _req_play, &req_task::play);
        break;
    }
}

void h264_play::set_progress_handle(void (*handle)(uint64_t))
{
    _req_pg_handle = handle;
}

//c binding interface here

void timer_task_loop()
{
    timer_task::get()->loop();
}
void timer_task_break_loop()
{
    timer_task::get()->break_loop();
}

void* h264_player_init(play_win_info_t* args, size_t size)
{
    void* opaque;

    if((opaque = new h264_play()) == 0)
        return 0;
    ((h264_play*)opaque)->init(args, size);
    return opaque;
}

int h264_player_play_or_pause(void* opaque)
{
    return ((h264_play*)opaque)->play_or_pause();
}

void h264_player_set_args(void* opaque, int lab, int dev, int mode, unsigned long long begin_pts, unsigned long long end_pts)
{
    ((h264_play*)opaque)->set_args(lab, dev, mode, begin_pts, end_pts);
}

int h264_player_check_args(void* opaque, int lab, int dev, int mode, unsigned long long begin_pts, unsigned long long end_pts)
{
    return ((h264_play*)opaque)->check_args(lab, dev, mode, begin_pts, end_pts);
}

void h264_player_get_args(void* opaque, int* lab, int* dev, int* mode, unsigned long long* begin_pts, unsigned long long* end_pts)
{
    ((h264_play*)opaque)->get_args(lab, dev, mode, begin_pts, end_pts);
}

int h264_player_seek(void* opaque, int percent)
{
    return ((h264_play*)opaque)->seek(percent);
}

int h264_player_pts_to_percent(void* opaque, unsigned long long pts)
{
    return ((h264_play*)opaque)->pts_to_percent(pts);
}

void h264_player_teardown(void* opaque)
{
    ((h264_play*)opaque)->teardown();
}

void h264_player_switch_mode(void* opaque, int lab, int dev, int mode, unsigned long long begin_pts, unsigned long long end_pts)
{
    ((h264_play*)opaque)->switch_mode(lab, dev, mode, begin_pts, end_pts);
}

void h264_player_set_progress_handle(void* opaque, void(*handle)(uint64_t))
{
    ((h264_play*)opaque)->set_progress_handle(handle);
}

const char* args_info_labs()
{
    args_info::get()->_keys_labs.c_str();
}

const char* args_info_devs()
{
    args_info::get()->_keys_devs.c_str();
}

const char* args_info_chn_range(int chn)
{
    return 0;
}

time_t mk_time(int y, int mon, int d, int h, int min, int s)
{
    return make_time(y, mon, d, h, min, s);
}

unsigned long long gen_default_pts(const time_t *time)
{
    return gen_pts(time);
}


#include <QApplication>
#include <QWidget>
#include <QResizeEvent>
#include "../ui/player_ui.h"
#ifdef __MINGW32__
#undef main /* Prevents SDL from overriding main() */
#endif
#include <QVBoxLayout>
#include <QPushButton>
#include <QPalette>
int main_(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget w,* player;
    w.resize(500, 600);
    QVBoxLayout* layout = new QVBoxLayout(&w);
    w.setLayout(layout);
    layout->addWidget(new QPushButton(&w));
    player = new QWidget(&w);
    player->resize(400, 300);
    layout->addWidget(player);
    layout->addWidget(new QPushButton(&w));
    w.show();

    //SDL_Init(SDL_INIT_VIDEO);
    basic_play* play = new sub_play();
    if(play->init((const void*)player->winId(), player->width(), player->height(),
               "tcp://10.3.13.92:1213") < 0)
    {
        return -1;
    }
    play->setup(0, 0, 0);

    std::thread{[&](){
            std::this_thread::sleep_for(std::chrono::seconds(3));
            play->term();

                }}.detach();

    QPalette pla(player->palette());
    pla.setColor(QPalette::Background, Qt::black);
    player->setAutoFillBackground(true);
    player->setPalette(pla);





    return a.exec();
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    player_ui player;
    player.showMaximized();

    /*
    basic_play* play = new sub_play();
    if(play->init((const void*)player.winId(), player.width(), player.height(), "tcp://10.3.13.92:1213") < 0)
        goto failed;
    if(play->setup(0, 0, 0) < 0)
        goto failed;
        */
    return a.exec();

}
