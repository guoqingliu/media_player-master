#-------------------------------------------------
#
# Project created by QtCreator 2018-05-10T10:57:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = media_player
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS __STDC_FORMAT_MACROS __STDC_CONSTANT_MACROS
QMAKE_CXXFLAGS += -std=c++0x -fpermissive



INCLUDEPATH += $$PWD/3rt/h264_play/include

LIBS += $$PWD/3rt/h264_play/lib/libh264_play.a

SOURCES += main.cpp\
        player.cpp \
    play_mode_switch_config.cpp

HEADERS  += player.h \
    play_mode_switch_config.h

FORMS    += player.ui \
    play_mode_switch_config.ui
