//
// Created by rio on 2/7/18.
//

#include <nanomsg/pubsub.h>
#include "nn_sub.h"

nn_sub::nn_sub(const char *url) :
        nn_helper(url, NN_SUB), _count(0){
    log("nn_sub", "url=%s", url);
}

int nn_sub::sub(const void *topic, size_t topic_size) {
    return set_opt(NN_SUB, NN_SUB_SUBSCRIBE, topic, topic_size);
}

int nn_sub::unsub(const void *topic, size_t topic_size) {
    return set_opt(NN_SUB, NN_SUB_UNSUBSCRIBE, topic, topic_size);
}

int nn_sub::get_sub(void *topic, size_t* topic_size) {
    return get_opt(NN_SUB, NN_SUB_SUBSCRIBE, topic, topic_size);
}

nn_sub::nn_sub(const args_info *args) :
    nn_sub(args->pub_url().c_str())
{
    int recv_timeout;

    recv_timeout = atoi(args->_sub_recv_timeout.c_str());
    if(set_opt(NN_SOL_SOCKET, NN_RCVTIMEO, &recv_timeout, sizeof(int)) < 0)
        log("set_opt", "NN_RCVTIMEO", "%s failed", args->_sub_recv_timeout.c_str());
}
