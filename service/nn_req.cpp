//
// Created by rio on 2/7/18.
//

#include <sys/time.h>
#include <nanomsg/reqrep.h>
#include "nn_req.h"

nn_req::nn_req(const char *url) :
        nn_helper(url, NN_REQ) {
    log("nn_req", "url=%s", url);
}

void nn_req::push_h264_file(const char *file) {
    uint8_t* buf;
    size_t buf_size;
    uint8_t spe[] = { 0, 0, 1 };
    uint8_t* begin,* end,* r_begin,* r_end;
    size_t pts = 0;
    media_server_req req;
    req.set__type(cmd_type::push_video);
    video* video = req.mutable__push_video();
    timeval cur_tv;
    timeval now_tv;
    gettimeofday(&cur_tv, 0);

    size_t req_count = 0;

    if(av_file_map(file, &buf, &buf_size, 0, 0) >= 0);
    {
        for(;!_break_loop;)
        {
            int frame_index = -2;
            begin = buf + 1;
            end = buf + 1 + sizeof(spe);

            for (; end < buf + buf_size - sizeof(spe) && !_break_loop;)
            {
                if (memcmp(spe, end, sizeof(spe)) == 0)
                {
                    r_begin = begin + sizeof(spe);
                    r_end = *(end - 1) == 0 ? end - 1 : end;
                    //print_buf(r_begin, r_end - r_begin);
                    //_cache.write(r_begin, r_end - r_begin, pts);
                    video->clear__items();
                    video_item *item = video->add__items();
                    video_key *key = item->mutable__key();
                    key->set__lab(0);
                    key->set__dev(0);
                    key->set__chn(0);
                    if(frame_index < 0)
                    {
                        pts = AV_NOPTS_VALUE;
                    }
                    else
                    {
                        cur_tv.tv_usec += (TIME_US_BASE / FRAME_RATE);
                        cur_tv.tv_sec += (cur_tv.tv_usec / TIME_US_BASE);
                        cur_tv.tv_usec %= TIME_US_BASE;
                        pts = gen_pts(&cur_tv);
                        gettimeofday(&now_tv, 0);
                        int interval = diff_time(&cur_tv, &now_tv);
                        if(interval > 0)
                            //usleep(interval);
                            std::this_thread::sleep_for(std::chrono::microseconds(interval));
                    }
                    ++frame_index;
                    item->set__pts(pts);
                    item->set__nalu(r_begin, r_end - r_begin);
                    for(int i = 0; i < 4; ++i)
                    {
                        key->set__chn(i);
                        request(req);
                    }
                    begin = end;
                    end += sizeof(spe);
                    pts += (AV_TIME_BASE / 25);
                    ++req_count;
                } else
                    ++end;
            }

            if (begin != end && !_break_loop)
            {
                r_begin = begin + sizeof(spe);
                r_end = buf + buf_size;
                //print_buf(r_begin, r_end - r_begin);
                //_cache.write(r_begin, r_end - r_begin, pts);
                video->clear__items();
                video_item *item = video->add__items();
                video_key *key = item->mutable__key();
                key->set__lab(0);
                key->set__dev(0);
                key->set__chn(0);
                if(frame_index < 0)
                {
                    pts = AV_NOPTS_VALUE;
                }
                else
                {
                    cur_tv.tv_usec += TIME_US_BASE / FRAME_RATE;
                    cur_tv.tv_sec += (cur_tv.tv_usec / TIME_US_BASE);
                    cur_tv.tv_usec %= TIME_US_BASE;
                    pts = gen_pts(&cur_tv);
                    gettimeofday(&now_tv, 0);
                    int interval = diff_time(&cur_tv, &now_tv);
                    if(interval > 0)
                        //usleep(interval);
                        std::this_thread::sleep_for(std::chrono::microseconds(interval));
                }
                ++frame_index;
                item->set__pts(pts);
                item->set__nalu(r_begin, r_end - r_begin);
                for(int i = 0; i < 4; ++i)
                {
                    key->set__chn(i);
                    request(req);
                }
                ++req_count;
            }
        }
        log("push h264 file", "req_count=%zu", req_count);
    }
}


void nn_req::request(media_server_req &req) {
    int ret;

    if((ret = req.ByteSize()) > _buf_max_size)
    {
        log("SerializeToArray too many bytes");
    }
    _buf_size = ret;
    if(req.SerializeToArray(_buf, _buf_max_size) < 0)
    {
        log("SerializeToArray failed");
        return;
    }

    if(send() < 0)
    {
        return;
    }
    recv();
}

nn_pollfd nn_req::pool_fd() {
    return nn_pollfd{ .fd = _sock, .events = NN_POLLOUT };
}

int nn_req::handle(const media_server_req &req, media_server_rep &rep) {
    if((_buf_size = req.ByteSize()) > _buf_max_size)
    {
        log("no enough mem");
        return -1;
    }

    if(!req.SerializeToArray(_buf, _buf_max_size))
        return -1;

    if(send() < 0)
        return -1;
    if(recv() < 0)
        return -1;
    if(!rep.ParseFromArray(_buf, _buf_size))
        return -1;
    return 0;
}

int nn_req::push_video(char lab, char dev, char chn, void *data, size_t data_size) {
    media_server_req req;
    media_server_rep rep;
    video* items;
    video_item* item;
    video_key* item_key;

    if((items = req.mutable__push_video()) == 0)
        return -1;
    if((item = items->add__items()) == 0)
        return -1;
    if((item_key = item->mutable__key()) == 0)
        return -1;

    item_key->set__chn(lab);
    item_key->set__dev(dev);
    item_key->set__chn(chn);
    item->set__nalu(data, data_size);

    req.set__type(cmd_type::push_video);

    if(handle(req, rep) < 0)
        return -1;

    if(rep._type() == cmd_type::push_video && rep.has__push_video() && rep._push_video()._status() == 0)
        return 0;
    return -1;
}

int nn_req::get_video_setup(uint64_t *id, char lab, char dev, char chn, uint64_t begin_pts, uint64_t end_pts) {
    media_server_req req;
    media_server_rep rep;

    media_server_req_get_video_setup* item;
    video_key* item_key;
    if((item = req.mutable__get_video_setup()) == 0)
        return -1;
    if((item_key = item->mutable__key()) == 0)
        return -1;
    item_key->set__lab(lab);
    item_key->set__dev(dev);
    item_key->set__chn(chn);
    item->set__begin_pts(begin_pts);
    item->set__end_pts(end_pts);

    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::setup);

    if(handle(req, rep) < 0)
        return -1;

    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::setup &&
       rep.has__get_video_setup() &&
       rep._get_video_setup()._status() == 0)
    {
        *id = rep._get_video_setup()._session();
        return 0;
    }

    return -1;
}

int nn_req::get_video_play(uint64_t id, void **data, size_t *data_size, uint64_t *pts) {
    media_server_req req;
    media_server_rep rep;
    media_server_req_get_video_play* item;

    if((item = req.mutable__get_video_play()) == 0)
        return -1;
    item->set__session(id);
    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::play);

    if(handle(req, rep) < 0)
        return -1;


    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::play &&
       rep.has__get_video_play() &&
       rep._get_video_play()._status() == 0)
    {
        const media_server_rep_get_video_play& play = rep._get_video_play();
        if(play.has__nalu() && play.has__pts())
        {
            *pts = play._pts();
            if((_buf_size = play._nalu().size()) > _buf_max_size)
                return -1;

            memcpy(_buf, &*play._nalu().begin(), _buf_size);
            *data = _buf;
            *data_size = _buf_size;
        } else
        {
            *data = 0;
            *data_size = 0;
            *pts = 0;
        }
        return 0;
    }
    return -1;
}

int nn_req::get_video_seek(uint64_t id, uint64_t pts) {
    media_server_req req;
    media_server_rep rep;
    media_server_req_get_video_seek* item;

    if((item = req.mutable__get_video_seek()) == 0)
        return -1;
    item->set__session(id);
    item->set__pts(pts);
    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::seek);

    if(handle(req, rep) < 0)
        return -1;


    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::seek &&
       rep.has__get_video_seek() &&
       rep._get_video_seek()._status() == 0)
        return 0;
    return -1;
}

int nn_req::get_video_teardown(uint64_t id) {
    media_server_req req;
    media_server_rep rep;
    media_server_req_get_video_teardown* item;

    if((item = req.mutable__get_video_teardown()) == 0)
        return -1;
    item->set__session(id);
    req.set__type(cmd_type::get_video);
    req.set__sub_type(cmd_sub_type::teardown);

    if(handle(req, rep) < 0)
        return -1;


    if(rep._type() == cmd_type::get_video &&
       rep._sub_type() == cmd_sub_type::teardown &&
       rep.has__get_video_teardown() &&
       rep._get_video_teardown()._status() == 0)
        return 0;
    return -1;
}

nn_req::nn_req(const args_info *args) :
    nn_req(args->rep_url().c_str())
{
    int send_timeout; //ms
    int recv_timeout;

    send_timeout = atoi(args->_req_send_timeout.c_str());
    recv_timeout = atoi(args->_req_recv_timeout.c_str());

    if(set_opt(NN_SOL_SOCKET, NN_SNDTIMEO, &send_timeout, sizeof(int)) < 0)
        log("set_opt", "NN_SNDTIMEO", "%s failed", args->_req_send_timeout.c_str());
    if(set_opt(NN_SOL_SOCKET, NN_RCVTIMEO, &recv_timeout, sizeof(int)) < 0)
        log("set_opt", "NN_RCVTIMEO", "%s failed", args->_req_recv_timeout.c_str());
}
