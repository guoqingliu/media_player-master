//
// Created by rio on 1/31/18.
//

#ifndef MEDIA_SERVER_H264_CACHE_KEY_H
#define MEDIA_SERVER_H264_CACHE_KEY_H

#include <cstdlib>
#include "../proto/media_server.pb.h"
#include "../util/util.h"

class h264_key
{
public:
    h264_key(const h264_key& key);
    h264_key(const media_server_pb::video_key& key);
    h264_key(int lab, int dev, int chn);
    void parse_cache_key(int* lab, int* dev, int* chn) const;
    int serialize_to_arrary(void* data, uint64_t data_size);
    friend bool operator<(const h264_key& l, const h264_key& r);
    friend bool operator==(const h264_key& l, const h264_key& r)
    {
        return memcmp(l._key, r._key, sizeof(l._key)) == 0;
    }
    static uint64_t key_size();
    void dump() const
    {
        int lab, dev, chn;

        parse_cache_key(&lab, &dev, &chn);

        default_log("h264_key", "dump", "lab=%d, dev=%d, chn=%d", lab, dev, chn);
    }
private:
    uint8_t _key[0x04];
};
#endif //MEDIA_SERVER_H264_CACHE_KEY_H
