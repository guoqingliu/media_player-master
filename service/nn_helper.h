//
// Created by rio on 1/30/18.
//

#ifndef MEDIA_SERVER_NN_UTIL_H
#define MEDIA_SERVER_NN_UTIL_H

#include <nanomsg/nn.h>
#include <cstdarg>
#include <thread>
#include "../proto/media_server.pb.h"
#include "../util/util.h"
#include "../util/args_info.h"
#include "../service/h264_key.h"

using namespace media_server_pb;
#define NN_HELPER_DEFAULT_BUF_SIZE (4 * 1024 * 1024)

class nn_helper
{
public:
    nn_helper(const char* url, int protocol,
              size_t buf_max_size = NN_HELPER_DEFAULT_BUF_SIZE);
    virtual ~nn_helper();
    int recv(void* buf, size_t buf_size);
    int recv();
    int send(const void* buf, size_t buf_size);
    int send();
    void loop();
    void break_loop();

    virtual void step();
    virtual void step(nn_pollfd* fd);
    virtual nn_pollfd pool_fd();
    void log(const char* what, const char* fmt = 0, ...);
    int set_opt(int level, int option, const void *optval, size_t optvallen);
    int get_opt(int level, int option, void *optval, size_t* optvallen);
    
protected:
    void* _buf;
    size_t _buf_size;
    size_t _buf_max_size;
    int _break_loop;
    char _where[0x20];
    int _sock;
    int _ep;
};

#endif //MEDIA_SERVER_NN_UTIL_H
