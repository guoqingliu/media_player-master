//
// Created by rio on 2/7/18.
//

#ifndef MEDIA_SERVER_NN_REQ_H
#define MEDIA_SERVER_NN_REQ_H


#include "nn_helper.h"

class nn_req : public nn_helper
{
public:
    nn_req(const char* url);
    nn_req(const args_info* args);
    nn_pollfd pool_fd() override;
    void push_h264_file(const char* file);
    void request(media_server_req& req);
    int handle(const media_server_req& req, media_server_rep& rep);
    int push_video(char lab, char dev, char chn, void* data, size_t data_size);
    int get_video_setup(uint64_t* id, char lab, char dev, char chn, uint64_t begin_pts, uint64_t end_pts);
    int get_video_play(uint64_t id, void** data, size_t* data_size, uint64_t* pts);
    int get_video_seek(uint64_t id, uint64_t pts);
    int get_video_teardown(uint64_t id);

private:
};
#endif //MEDIA_SERVER_NN_REQ_H
