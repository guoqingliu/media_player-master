#include "play_mode_switch_config.h"
#include "ui_play_mode_switch_config.h"
#include <h264_play.h>
static void gen_pts_range_by_qdate(const QDate* date, unsigned long long* begin_pts, unsigned long long* end_pts)
{
    int year;
    int month;
    int day;

    time_t begin_time;
    time_t end_time;

    year = date->year();
    month = date->month();
    day = date->day();

    begin_time = mk_time(year, month, day, 0, 0, 0);
    end_time = mk_time(year, month, day, 23, 59, 59);

    *begin_pts = gen_default_pts(&begin_time);
    *end_pts = gen_default_pts(&end_time);
}

static void init_cb_by_string(QComboBox* cb, const std::string& str)
{
    QString qstr = QString::fromStdString(str);
    QStringList strlist= qstr.split(",");
   foreach(QString item, strlist)
   {
       cb->addItem(item);
   }
}

play_mode_switch_config::play_mode_switch_config(QWidget *parent, void(*post_handle)(void*)) :
    QWidget(parent), _post_handle(post_handle),
    ui(new Ui::play_mode_switch_config)
{
    std::string labs;
    std::string devs;
    ui->setupUi(this);
    ui->de_date->setDate(QDate::currentDate());

    labs = args_info_labs();
    devs = args_info_devs();
    init_cb_by_string(ui->cb_lab, labs);
    init_cb_by_string(ui->cb_dev, devs);

    _flag = 0;
    ui->cb_lab->setCurrentIndex(0);
    ui->cb_dev->setCurrentIndex(0);
    ui->cb_mode->setCurrentIndex(0);
    ui->de_date->setDate(QDate::currentDate());
}

play_mode_switch_config::~play_mode_switch_config()
{
    delete ui;
}

void play_mode_switch_config::on_cb_mode_currentIndexChanged(int index)
{
    ui->de_date->setEnabled(index != 0);
}

void play_mode_switch_config::on_pb_ok_clicked()
{
    QDate date = ui->de_date->date();
    _args._lab = ui->cb_lab->currentText().toInt();
    _args._dev = ui->cb_dev->currentText().toInt();
    _args._mode = ui->cb_mode->currentIndex();
    gen_pts_range_by_qdate(&date, &_args._begin_pts, &_args._end_pts);
    _flag = 1;
    this->close();
    _post_handle(this->parent());
}

void play_mode_switch_config::on_pb_cancel_clicked()
{
    this->close();
}
