//
// Created by rio on 4/16/18.
//

#ifndef MEDIA_SERVER_XML_CONFIGURE_H
#define MEDIA_SERVER_XML_CONFIGURE_H

#include <string>
#include <tinyxml2.h>

#define PARSE_NODE(args, root, clss, item) \
    if(parse_from_node(root, #clss, #item, args->_##clss##_##item) < 0) \
        return -1;
#define SAVE_NODE(args, root, clss, item) \
    if(save_to_node(root, #clss, #item, args->_##clss##_##item) < 0) \
        return -1;

using tinyxml2::XMLDocument;
using tinyxml2::XMLElement;
using tinyxml2::XMLError;
class args_info
{
private:
    args_info(){
    }
    ~args_info()
    {
        save_to_xml();
    }

    int parse_from_node(const XMLElement* base, const char* clss, const char* item, std::string& des)
    {
        const XMLElement* pos;

        pos = base;

        if((pos = pos->FirstChildElement(clss)) == 0)
            return -1;
        if((pos = pos->FirstChildElement(item)) == 0)
            return -1;
        des = pos->GetText();
        return 0;
    }

    int save_to_node(const XMLElement* base, const char* clss, const char* item, const std::string& des)
    {
        const XMLElement* pos;

        pos = base;

        if((pos = pos->FirstChildElement(clss)) == 0)
            return -1;
        if((pos = pos->FirstChildElement(item)) == 0)
            return -1;
        pos->SetText(des.c_str());
        return 0;
    }
public:
    int parse_from_xml()
    {
        const char* xml_file = "configure.xml";
        XMLDocument doc;
        const XMLElement* root;
        if(doc.LoadFile(xml_file) != XMLError::XML_SUCCESS)
            return -1;
        if((root = doc.RootElement()) == 0)
            return -1;

        PARSE_NODE(this, root, pub, port);

        PARSE_NODE(this, root, rep, port);

        PARSE_NODE(this, root, url, cur);
        PARSE_NODE(this, root, url, set);

        PARSE_NODE(this, root, sub, recv_timeout);
        PARSE_NODE(this, root, req, recv_timeout);
        PARSE_NODE(this, root, req, send_timeout);

        PARSE_NODE(this, root, keys, labs);
        PARSE_NODE(this, root, keys, devs);
        PARSE_NODE(this, root, keys, cur_lab);
        PARSE_NODE(this, root, keys, cur_dev);
        PARSE_NODE(this, root, keys, cur_mode);

        return 0;
    }

    int save_to_xml()
    {
        const char* xml_file = "configure.xml";
        XMLDocument doc;
        const XMLElement* root;
        if(doc.LoadFile(xml_file) != XMLError::XML_SUCCESS)
            return -1;
        if((root = doc.RootElement()) == 0)
            return -1;

        SAVE_NODE(this, root, pub, port);

        SAVE_NODE(this, root, rep, port);
        SAVE_NODE(this, root, url, cur);
        SAVE_NODE(this, root, url, set);

        SAVE_NODE(this, root, sub, recv_timeout);
        SAVE_NODE(this, root, req, recv_timeout);
        SAVE_NODE(this, root, req, send_timeout);

        SAVE_NODE(this, root, keys, labs);
        SAVE_NODE(this, root, keys, devs);
        SAVE_NODE(this, root, keys, cur_lab);
        SAVE_NODE(this, root, keys, cur_dev);
        SAVE_NODE(this, root, keys, cur_mode);

        doc.SaveFile(xml_file);
        return 0;
    }


public:
    static args_info* get(){
        static args_info* _g_args = 0;
        if(_g_args == 0)
        {
            _g_args = new args_info();
            if(_g_args->parse_from_xml() < 0)
            {
                delete _g_args;
                _g_args = 0;
            }
        }
        return _g_args;

    }
    std::string rep_url()
    {
        char buf[0x40];
        snprintf(buf, sizeof(buf), "tcp://%s:%s", _url_cur.c_str(), _rep_port.c_str());
        return buf;
    }
    std::string pub_url()
    {
        char buf[0x40];
        snprintf(buf, sizeof(buf), "tcp://%s:%s", _url_cur.c_str(), _pub_port.c_str());
        return buf;
    }
public:
    std::string _pub_port;

    std::string _rep_port;

    std::string _url_cur;
    std::string _url_set;

    std::string _sub_recv_timeout;
    std::string _req_recv_timeout;
    std::string _req_send_timeout;

    std::string _keys_labs;
    std::string _keys_devs;
    std::string _keys_cur_lab;
    std::string _keys_cur_dev;
    std::string _keys_cur_mode;
};
/*
<?xml version="1.0" encoding="utf-8"?>
<config>
    <pub>
        <port>1213</port>
        <last_url></last_url>
        <urls></urls>
    </pub>
    <rep>
        <port>1214</port>
        <last_url></last_url>
        <urls></urls>
    </rep>
    <sub>
        <recv_timeout>40</recv_timeout>
    </sub>
    <req>
        <recv_timeout>200</recv_timeout>
        <send_timeout>200</send_timeout>
    </req>
    <keys>
        <labs>0,1,2,3,4,5</labs>
        <devs>0,1,2,3,4,5</devs>
        <last_lab></last_lab>
        <last_dev></last_dev>
        <last_mode></last_mode>
    </keys>
</config>
*/
#endif //MEDIA_SERVER_XML_CONFIGURE_H
