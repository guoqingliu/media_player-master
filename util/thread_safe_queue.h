//
// Created by rio on 4/9/18.
//

#ifndef MEDIA_SERVER_THREAD_SAFE_QUEUE_H
#define MEDIA_SERVER_THREAD_SAFE_QUEUE_H

#include <queue>
#include <condition_variable>
#include <mutex>

template<typename T>
class thread_safe_queue{
private:
    using queue_t = std::queue<T>;
    using value_type= typename queue_t::value_type;
    using container_type = typename queue_t::container_type;
    mutable std::mutex _mtx;
    mutable std::condition_variable _cond;
    queue_t _que;
public:
    thread_safe_queue()=default;
    thread_safe_queue(const thread_safe_queue&)=delete;
    thread_safe_queue& operator=(const thread_safe_queue&)=delete;
    template<typename _InputIterator>
    thread_safe_queue(_InputIterator first, _InputIterator last){
        for(auto itor=first;itor!=last;++itor){
            _que.push(*itor);
        }
    }
    explicit thread_safe_queue(const container_type &c):_que(c){}
    thread_safe_queue(std::initializer_list<value_type> list):thread_safe_queue(list.begin(),list.end()){
    }
    void push(const value_type &new_value){
        std::lock_guard<std::mutex>lk(_mtx);
        _que.push(std::move(new_value));
        _cond.notify_one();
    }
    value_type wait_and_pop(){
        std::unique_lock<std::mutex>lk(_mtx);
        _cond.wait(lk,[this]{return !this->_que.empty();});
        auto value=std::move(_que.front());
        _que.pop();
        return value;
    }
    bool try_pop(value_type& value){
        std::lock_guard<std::mutex>lk(_mtx);
        if(_que.empty())
            return false;
        value=std::move(_que.front());
        _que.pop();
        return true;
    }
    bool try_pop(value_type& value, size_t ms){
        std::unique_lock<std::mutex>lk(_mtx);
        if(_cond.wait_for(lk, std::chrono::milliseconds(ms), [&](){ return !_que.empty(); }))
        {
            value = std::move(_que.front());
            _que.pop();
            return true;
        }
        return false;
    }
    auto empty() const->decltype(_que.empty()) {
        std::lock_guard<std::mutex>lk(_mtx);
        return _que.empty();
    }
    auto size() const->decltype(_que.size()){
        std::lock_guard<std::mutex>lk(_mtx);
        return _que.size();
    }
};
#endif //MEDIA_SERVER_THREAD_SAFE_QUEUE_H
