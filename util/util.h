//
// Created by rio on 1/31/18.
//

#ifndef MEDIA_SERVER_LOG_H
#define MEDIA_SERVER_LOG_H

#include <cassert>
#include <cstdio>
#include <cstdarg>
#include <mutex>
//ffmepg
extern "C" {
#include <libavutil/timestamp.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/file.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/dict.h>
#include <libavutil/log.h>
}

#define _av_ts2str(ts) av_ts_make_string((char*)(char[AV_TS_MAX_STRING_SIZE]){0}, ts)
#define _av_ts2timestr(ts, tb) av_ts_make_time_string((char*)(char[AV_TS_MAX_STRING_SIZE]){0}, ts, tb)
#define _av_err2str(errnum) \
    av_make_error_string((char*)(char[AV_ERROR_MAX_STRING_SIZE]){0}, AV_ERROR_MAX_STRING_SIZE, errnum)

//log

class log_basic
{
public:
    virtual void log(const char* where, const char* what, const char* fmt = 0, ...) = 0;
    virtual void vlog(const char* where, const char* what, const char* fmt, va_list va) = 0;
};

class console_log : public log_basic
{
public:
    void log(const char* where, const char* what, const char* fmt = 0, ...) override ;
    void vlog(const char* where, const char* what, const char* fmt, va_list va) override ;

private:
    std::mutex _mtx;
};
extern console_log _g_default_log;
#define default_log(...) _g_default_log.log(__VA_ARGS__)
#define default_vlog(where, what, fmt, va) _g_default_log.vlog(where, what, fmt, va)

void print_buf(const uint8_t* buf, int buf_size);
#define TIME_US_BASE 1000000
#define TIME_MS_BASE 1000
#define TIME_BASE 1200000
#define FRAME_RATE 25.5

time_t make_time(int y, int mon, int d, int h, int min, int s);
int diff_time(const timeval* tv1, const timeval* tv2);
uint64_t gen_pts(uint32_t sec, uint32_t ms, const AVRational* time_base = 0);
uint64_t gen_pts(const time_t* time, const AVRational* time_base = 0);
uint64_t gen_pts(const timeval* tv, const AVRational* time_base = 0);

void parse_pts(uint64_t pts, time_t* time, const AVRational* time_base = 0);
void parse_pts(uint64_t pts, timeval* tv, const AVRational* time_base = 0);

uint64_t get_now_us();

#endif //MEDIA_SERVER_LOG_H



