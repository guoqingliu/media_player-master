#include "player.h"
#include "ui_player.h"
#include "play_mode_switch_config.h"
#include <h264_play.h>
#include <stdio.h>
#include <QDesktopWidget>

static player* _g_player = 0;

void progress_impl(unsigned long long pts)
{
    if(_g_player)
        _g_player->progress_handle_impl(pts);
}

static void h264_source_info_init(play_win_info_t* info, QWidget* w)
{
    info->_win_id = (void*)w->winId();
    info->_sws_width = w->width();
    info->_sws_height = w->height();
}

player::player(QWidget *parent) :
    QWidget(parent), _config_win(0),
    ui(new Ui::player)
{
    //this->setStyleSheet("background-color:rgb(255,34,198)");
    setWindowTitle("media player");
    _g_player = this;
    ui->setupUi(this);
    ui->pb_seek->setVisible(false);

    init_chn_control();
    this->setAutoFillBackground(true);
    /*
    QPalette palette;
    QPixmap pixmap = QPixmap("bg.png").scaled(this->size());
    palette.setBrush(QPalette::Window, QBrush(pixmap));
    this->setPalette(palette);
    */
    play_win_info_t args[0x04];
    h264_source_info_init(args, ui->qw_play0);
    h264_source_info_init(args + 1, ui->qw_play1);
    h264_source_info_init(args + 2, ui->qw_play2);
    h264_source_info_init(args + 3, ui->qw_play3);
    _play = h264_player_init(args, sizeof(args) / sizeof(args[0]));
    if(_play == 0)
        throw;
    h264_player_set_progress_handle(_play, progress_impl);
    ui->pb_play->setEnabled(false);
    ui->pb_pause->setEnabled(true);
    timer_task_loop();
}

player::~player()
{
    delete ui;
}


void player::on_pb_seek_valueChanged(int value)
{
    h264_player_seek(_play, value);
}

void player_config_post_handle(void *opaque)
{
    ((player*)opaque)->config_post_handle_impl();
}

void player::on_pb_switch_clicked()
{
    if(_config_win == 0)
    {
        _config_win = new play_mode_switch_config(this, player_config_post_handle);
        _config_win->move(this->width() / 2, this->height() / 2);
    }
    else
        _config_win->reset_flag();
    _config_win->show();
}

void player::closeEvent(QCloseEvent *)
{
    timer_task_break_loop();
}
#include<QMessageBox>
#include<QMouseEvent>
void player::mouseReleaseEvent(QMouseEvent *event)
{
    return;
    QString info;
    char buf[0x20];
    QPoint pos = event->globalPos();
    pos = ui->qw_content->mapFromGlobal(pos);
    info += itoa(pos.x(), buf, 10);
    info += ",";
    info += itoa(pos.y(), buf, 10);
   QMessageBox::information(this, "", info);
}

static void init_chn_control_impl(QWidget* control, const char* range)
{
    QString r(range);
    QStringList range_list = r.split(",");
    if(range_list.size() < 4)
        return;

    int x, y, w, h;
    QPoint pos;
    x = range_list.at(0).toInt();
    y = range_list.at(1).toInt();
    w = range_list.at(2).toInt();
    h = range_list.at(3).toInt();
    pos.setX(x);
    pos.setY(y);
    //pos = control->parentWidget()->mapFromGlobal(pos);
    control->resize(w, h);
    control->move(pos);
    printf("range=%s\n", range);
}

void player::init_chn_control()
{
#if 1
    int width, height;
    int r_w, r_h;
    int x, y, w, h;
    int cur_h;
    double w_ratio, h_ratio, ratio;
    char buf[0x100];

    r_w = 1600 * 2;
    r_h = 1200 + 768 + 800;

    setWindowFlags(Qt::WindowCloseButtonHint);
    showMaximized();
    setFixedSize(this->width(), this->height());
    width = ui->qw_content->width() - 30;
    height = ui->qw_content->height() - 30;

    w_ratio = width * 1.0 / r_w;
    h_ratio = height * 1.0 / r_h;

    ratio = w_ratio < h_ratio ? w_ratio : h_ratio;

    cur_h = 0;

    w = ratio * 1024;
    h = ratio * 768;
    x = (width - w) / 2;
    x += 10;
    y = 0;
    y += 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play1, buf);
    cur_h += (y + h);

    w = ratio * 1600;
    h = ratio * 1200;
    x = (width - w * 2) / 2;
    x += 10;
    y = cur_h + 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play2, buf);
    x += w;
    x += 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play3, buf);
    cur_h = (y + h);

    w = ratio * 800;
    h = ratio * 800;
    x = (width - w) / 2;
    y = cur_h + 10;
    x += 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play0, buf);
 #else

    int width, height;
    int r_w, r_h;
    int x, y, w, h;
    int cur_h;
    double w_ratio, h_ratio, ratio;
    char buf[0x100];

    r_w = 1920 * 2;
    r_h = 1080 + 1080 + 1080;

    setWindowFlags(Qt::WindowCloseButtonHint);
    showMaximized();
    setFixedSize(this->width(), this->height());
    width = ui->qw_content->width() - 30;
    height = ui->qw_content->height() - 30;

    w_ratio = width * 1.0 / r_w;
    h_ratio = height * 1.0 / r_h;

    ratio = w_ratio < h_ratio ? w_ratio : h_ratio;

    cur_h = 0;

    w = ratio * 1920;
    h = ratio * 1080;
    x = (width - w) / 2;
    x += 10;
    y = 0;
    y += 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play1, buf);
    cur_h += (y + h);

    w = ratio * 1920;
    h = ratio * 1080;
    x = (width - w * 2) / 2;
    x += 10;
    y = cur_h + 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play2, buf);
    x += w;
    x += 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play3, buf);
    cur_h = (y + h);

    w = ratio * 1920;
    h = ratio * 1080;
    x = (width - w) / 2;
    y = cur_h + 10;
    x += 10;
    sprintf(buf, "%d,%d,%d,%d", x, y, w, h);
    init_chn_control_impl(ui->qw_play0, buf);
#endif
}

void player::init_win()
{

}

void player::config_post_handle_impl()
{
    play_args_t* args = _config_win->get_args();
    if(args == 0)
        return;
    h264_player_switch_mode(_play, args->_lab, args->_dev, args->_mode, args->_begin_pts, args->_end_pts);

}

void player::progress_handle_impl(unsigned long long pts)
{
    int step_value;

    step_value= h264_player_pts_to_percent(_play, pts);
    this->ui->pb_seek->setValue(step_value);
}

void player::on_pb_play_clicked()
{
    h264_player_play_or_pause(_play);
    ui->pb_play->setEnabled(false);
    ui->pb_pause->setEnabled(true);
}

void player::on_pb_pause_clicked()
{
    h264_player_play_or_pause(_play);
    ui->pb_play->setEnabled(true);
    ui->pb_pause->setEnabled(false);
}
