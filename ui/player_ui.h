#ifndef PLAYER_UI_H
#define PLAYER_UI_H

#include <QWidget>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QSlider>
#include <QFile>
#include <QTimer>
#include "player_config_ui.h"
#include "play/h264_play.h"
namespace Ui {
class player_ui;
}

class player_slider;


class player_ui : public QWidget
{
    Q_OBJECT

public:
    explicit player_ui(QWidget *parent = 0);
    ~player_ui();
    void init(int mode);
    void init_new_player(QWidget *player);

    inline void run(basic_play *play,QWidget *player,int lab,int dev,int chn)
    {
        QString tmp = player_config_ui::url;
        std::string str = tmp.toStdString();
        const char *url = str.c_str();
        if(play->init((const void*) player->winId(),  player->width(),  player->height(),url) < 0)
            //goto failed;
            qDebug("play init error");
        if(play->setup(lab, dev, chn) < 0)
            //goto failed;
            qDebug("play setup error");
    }

    friend class player_window;
    friend class player_slider;
protected:
    void mouseMoveEvent(QMouseEvent *) override;
    void deleter();
    void run_play();
private slots:
    void on_pb_play_pause_clicked();

    void on_pb_open_clicked();

private:
    Ui::player_ui *ui;
    player_config_ui* _config_ui;
    QSlider *_slider;
    int _init_play;
    enum {PLAY_SIZE = 4};

private:
    basic_play *_play[PLAY_SIZE];
    QWidget *_players[PLAY_SIZE];
    std::vector<h264_key> _key_vec;          //note
    int _player_size;
    QWidget *new_player; 
    basic_play *new_play;
    int _mode;
    QLabel *end_pts;
    unsigned long long _begin_pts;
    unsigned long long _end_pts;

};


class player_window:public QWidget
{
    Q_OBJECT

public:
    player_window(QWidget *main_ui, int id = -1,QWidget *parent = 0):
        QWidget(parent),_id(id)
    {
        _main_ui = main_ui;

    }

protected:
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void handle_full_screen();
    void handle_mini_screen();

private:
    static int flag;
    player_ui *_main_ui;

    int _id;
};


class player_slider:public QSlider
{
    Q_OBJECT

public:
    player_slider(QWidget *parent):
        QSlider(Qt::Horizontal,parent),_main_ui(parent),_timer(new QTimer(this))
    {
        connect(this,SIGNAL(sliderMoved(int)),this,SLOT(on_move_slider(int)));
        connect(this,SIGNAL(sliderReleased()),this,SLOT(on_release_slider()));
        connect(_timer,SIGNAL(timeout()),this,SLOT(on_time_out()));

        _timer->start(1000);
    }
    void mousePressEvent(QMouseEvent *ev) override;
    void sec_to_date(int sec,int *hour,int *min,int *second)
    {

        *hour=sec/3600;
        *min=(sec%3600)/60;
        *second=(sec%3600)%60;
    }
public slots:
    void on_move_slider(int pos);
    void on_release_slider();
    void on_time_out();

private:
    void set_seek(int pos);

private:
    player_ui * _main_ui;
    QTimer *_timer;
};


#endif // PLAYER_UI_H
