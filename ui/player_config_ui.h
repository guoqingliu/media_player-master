#ifndef PLAYER_CONFIG_UI_H
#define PLAYER_CONFIG_UI_H

#include <QDialog>
#include <QDate>
namespace Ui {
class player_config_ui;
}

class player_config_ui : public QDialog
{
    Q_OBJECT

public:
    explicit player_config_ui(QWidget *parent = 0);
    void init();
    void save();
    ~player_config_ui();
    void closeEvent(QCloseEvent *event)override;
    static QString url;
    static int flag;
private slots:
    void on_pb_url_test_clicked();

    void on_pb_ok_clicked();

    void on_pb_cancel_clicked();
    void on_pb_current_mode(int);
private:
    Ui::player_config_ui *ui;

public:
    int _cur_lab;
    int _cur_dev;
    int _cur_mode;
    QDate _cur_date;

};

#endif // PLAYER_CONFIG_UI_H
