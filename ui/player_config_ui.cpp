#include "player_config_ui.h"
#include "ui_player_config_ui.h"
#include "../util/args_info.h"
#include "../service/nn_sub.h"
#include <QCloseEvent>
#include <QMessageBox>
#pragma execution_character_set("utf-8")

int player_config_ui::flag = 0;
QString player_config_ui::url = "";
player_config_ui::player_config_ui(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::player_config_ui)
{
    ui->setupUi(this);
    init();

    connect(this->ui->cb_modes,SIGNAL(currentIndexChanged(int)),this,SLOT(on_pb_current_mode(int)));
}

int test_connectable(const char* url)
{
    args_info* args = args_info::get();
    if(args == 0)
        return -1;
    std::string prev_url = args->_url_cur;

    args->_url_cur = url;
    try
    {
        nn_sub sub(args);
        sub.sub("",0);
        if(sub.test_connetable() < 0)
        {
            args->_url_cur = prev_url;
            return -1;
        }
    }catch(...)
    {
        args->_url_cur = prev_url;
        return -1;
    }
    args->_url_cur = prev_url;
    return 0;
}

static void init_cb_by_string(QComboBox* cb, const std::string& args, const std::string& arg)
{
    int index;
    for(index = cb->count() - 1; index >= 0; --index)
        cb->removeItem(index);

    QString qargs = QString::fromStdString(args);
    QString qarg = QString::fromStdString(arg);
    QStringList qargs_list = qargs.split(",");

    foreach(QString item, qargs_list)
    {
       cb->addItem(item);
    }

    index = 0;
    foreach(QString item, qargs_list)
    {
        if(item == qarg)
            break;
        ++index;
    }

    if(index == qargs_list.size())
        index = 0;

    if(qargs_list.size())
        cb->setCurrentIndex(index);
}

static void test_connectable(QComboBox* cb)
{
    for(int i = 0; i < cb->count(); ++i)
    if(test_connectable(cb->itemText(i).toStdString().c_str()) < 0)
        cb->setItemData(i, QColor(Qt::red), Qt::ForegroundRole);
    else
        cb->setItemData(i, QColor(Qt::blue), Qt::ForegroundRole);
}
void player_config_ui::init()
{
    args_info* args = args_info::get();
    if(args == 0)
        return;
    init_cb_by_string(ui->cb_urls, args->_url_set, args->_url_cur);
    //test_connectable(ui->cb_urls);
    init_cb_by_string(ui->cb_devs, args->_keys_devs, args->_keys_cur_dev);
    init_cb_by_string(ui->cb_devs, args->_keys_devs, args->_keys_cur_dev);
    ui->cb_modes->setCurrentIndex(atoi(args->_keys_cur_mode.c_str()));
    ui->cw_date->setSelectedDate(QDate::currentDate());
    _cur_lab = -1;
    _cur_dev = -1;
    _cur_date = QDate();
    _cur_mode = -1;

    if(ui->cb_modes->currentIndex())
        ui->cw_date->setEnabled(true);

}

void save_cb(QComboBox* cb, std::string& set, std::string& cur)
{
    int index;

    set = "";
    for(index = 0; index < cb->count(); ++index)
    {
        if(cb->itemText(index) == cb->currentText())
            break;
    }
    if(index == cb->count())
    {
        set += cb->currentText().toStdString();
        set += ",";
    }
    for(index = 0; index < cb->count(); ++index)
    {
        set += cb->itemText(index).toStdString();
        set += ",";
    }

    if(cb->count() > 0)
        set.resize(set.size() - 1);

    cur = cb->currentText().toStdString();
}

void player_config_ui::save()
{
    args_info* args = args_info::get();
    char buf[0x20];

    if(args == 0)
        return;
    int lab = atoi(ui->cb_labs->currentText().toStdString().c_str());
    int dev = atoi(ui->cb_devs->currentText().toStdString().c_str());
    int mode = ui->cb_modes->currentIndex();
    QDate date = ui->cw_date->selectedDate();
    if(lab != _cur_lab || dev != _cur_dev || mode != _cur_mode)
    {
        _cur_lab = lab,_cur_dev = dev,_cur_mode = mode;
        if(_cur_date != date)
        {
            _cur_date = date;
        }
        flag = 2;
    }
    if(_cur_mode)  //history
    {
        if(_cur_date != date)
        {
            _cur_date = date;
            flag = 2;
        }
    }

    save_cb(ui->cb_urls, args->_url_set, args->_url_cur);
    save_cb(ui->cb_labs, args->_keys_labs, args->_keys_cur_lab);
    save_cb(ui->cb_devs, args->_keys_devs, args->_keys_cur_dev);
    args->_keys_cur_mode = itoa(ui->cb_modes->currentIndex(), buf, 10);
    args->save_to_xml();
}

player_config_ui::~player_config_ui()
{
    delete ui;
}

void player_config_ui::on_pb_url_test_clicked()
{
    QString url = ui->cb_urls->currentText();
    url = url.trimmed();
    if(test_connectable(url.toStdString().c_str()) == 0)
    {
        QMessageBox::information(this, QString::fromUtf8("连接测试"), QString::fromUtf8("连接成功"));

        //set enable
        ui->cb_devs->setEnabled(true);
        ui->cb_labs->setEnabled(true);
        ui->pb_ok->setEnabled(true);

    }
    else
    {
         QMessageBox::StandardButton ok = QMessageBox::information(this, QString::fromUtf8("连接测试"), QString::fromUtf8("连接失败"));
         if(ok == QMessageBox::StandardButton::Ok)
         {
            //ui->pb_ok->setEnabled(false);
         }
    }
}

void player_config_ui::on_pb_ok_clicked()
{
    save();

    this->ui->cb_urls->setEnabled(false);
    this->ui->pb_url_test->setEnabled(false);
    url = QString("tcp://").append(ui->cb_urls->currentText()).append(":1213");
    this->hide();
}

void player_config_ui::on_pb_cancel_clicked()
{
    flag = 0;
    this->hide();
}

void player_config_ui::on_pb_current_mode(int index)
{
    if(index)
        ui->cw_date->setEnabled(true);
    else
    {
        ui->cw_date->setEnabled(false);
    }
}


void player_config_ui::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton button;
    button = QMessageBox::question(this, QString::fromUtf8("配置"),
        QString::fromUtf8("确认退出配置吗？"),QMessageBox::Yes | QMessageBox::No);

    if (button == QMessageBox::No)
    {
        event->ignore();
    }
    else if (button == QMessageBox::Yes)
    {
        event->accept();
    }



}
