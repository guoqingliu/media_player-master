#include "player_ui.h"
#include "ui_player_ui.h"
#include <QApplication>
#include <QMouseEvent>
#include <QGridLayout>
#include <QLayout>

int player_window::flag = 0;

static void gen_pts_range_by_qdate(const QDate* date, unsigned long long* begin_pts, unsigned long long* end_pts)
{
    int year;
    int month;
    int day;

    time_t begin_time;
    time_t end_time;

    year = date->year();
    month = date->month();
    day = date->day();

    begin_time = mk_time(year, month, day, 0, 0, 0);
    end_time = mk_time(year, month, day, 23, 59, 59);
    *end_pts = gen_default_pts(&end_time);
    *begin_pts = gen_default_pts(&begin_time);

}


static init_pb_icon(QPushButton* pb, const char* icon_path)
{
    pb->setIcon(QPixmap(icon_path));
}

static init_qw_ctrl(QWidget* qw)
{
    QPalette pal(qw->palette());
    pal.setColor(QPalette::Background, Qt::black);
    qw->setAutoFillBackground(true);
    qw->setPalette(pal);
}

void player_window::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(!_main_ui->_init_play)
    {
        QMessageBox::information(this, QString::fromUtf8("配置参数"), QString::fromUtf8("请配置参数"));
    }
    else
    {
        if(flag == 0)   //handle full screen
        {
            handle_full_screen();
        }
        else if(flag == 1)
        {
            handle_mini_screen();
        }
    }


}

void player_ui::init_new_player(QWidget *player)
{
    //size
    QDesktopWidget *deskWgt = QApplication::desktop();

    player->setMinimumSize(ui->widget_play->width(),ui->widget_play->height());

    //bgcolor
    QPalette pa(player->palette());
    pa.setColor(QPalette::Background,Qt::black);
    player->setAutoFillBackground(true);
    player->setPalette(pa);

    ui->horizontalLayout_top->addWidget(player);
}
void player_window::handle_full_screen()
{
    player_ui *main_ui = _main_ui;

    int player_size = main_ui->_player_size;
    //destructor threads
    int status = main_ui->_play[0]->get_status();
    for(int index = 0;index < player_size;++index)
    {
        _main_ui->_play[index]->term();
    }

    //show new_player
    int id = this->_id;
    main_ui->deleter();
    main_ui->new_player = new player_window(main_ui,-1,main_ui->ui->widget_play);

    main_ui->init_new_player(main_ui->new_player);
    main_ui->new_player->show();

    int lab,dev,chn;
    main_ui->_key_vec[id].parse_cache_key(&lab,&dev,&chn);
    main_ui->run(main_ui->new_play,main_ui->new_player,lab,dev,chn);
    if(status & PS_PAUSE)
    {
        init_pb_icon(main_ui->ui->pb_play_pause, "./icon/play.png");
        main_ui->new_play->play_or_pause();
    }

    //set flag
    flag = 1;

}

void player_window::handle_mini_screen()
{
    player_ui *main_ui = _main_ui;
    int player_size = main_ui->_player_size;
    //destructor thread
    int status = main_ui->new_play->get_status();
    _main_ui->new_play->term();

    QLayoutItem *item;
    while(item=_main_ui->ui->horizontalLayout_top->takeAt(0))
    {
        _main_ui->ui->horizontalLayout_top->removeItem(item);
        delete item;
    }

    delete main_ui->new_player;
    main_ui->new_player = 0;
    // set players
    main_ui->init(main_ui->_mode);

    int lab,dev,chn;
    for(int index = 0;index < player_size;++index)
    {
        main_ui->_key_vec[index].parse_cache_key(&lab,&dev,&chn);
        main_ui->run(main_ui->_play[index],main_ui->_players[index],lab,dev,chn);
        if(status &PS_PAUSE)
        {
            init_pb_icon(main_ui->ui->pb_play_pause, "./icon/play.png");
            main_ui->_play[index]->play_or_pause();
        }
    }

    flag = 0;
}

void player_slider::set_seek(int pos)
{

    double ratio = pos/(double)this->maximum();
    unsigned long long pts = (_main_ui->_end_pts - _main_ui->_begin_pts)*ratio +_main_ui->_begin_pts;
    if(_main_ui->new_player && !_main_ui->new_player->isHidden())
    {
        _main_ui->new_play->seek(pts);
    }
    else
    {
        for(int index = 0;index < _main_ui->_player_size ; ++index)
        {
            _main_ui->_play[index]->seek(pts);
        }
    }
}

void player_slider::on_move_slider(int pos)
{

}

void player_slider::on_release_slider()
{
    int pos = this->value();
    set_seek(pos);

}

void player_slider::mousePressEvent(QMouseEvent *ev)
{
    QSlider::mousePressEvent(ev);
    double pos = ev->pos().x() / (double)width();
    setValue(pos * (maximum() - minimum()) + minimum());

    set_seek(this->value());
}

void player_slider::on_time_out()
{
    unsigned long long cur_pts;
    if(!_main_ui->_init_play)
    {
        return ;
    }
    if(_main_ui->new_player && !_main_ui->new_player->isHidden())
    {
        cur_pts = _main_ui->new_play->cur_pts();
    }
    else if(_main_ui->_play[0] &&!_main_ui->_players[0]->isHidden())
    {
        cur_pts = _main_ui->_play[0]->cur_pts();
    }

    time_t cur_time;
    parse_pts(cur_pts,&cur_time);
    tm cur_tm;
    localtime_s(&cur_tm,&cur_time);
    QTime cur_date(cur_tm.tm_hour,cur_tm.tm_min,cur_tm.tm_sec);
    _main_ui->ui->lbl_cur_pts->setText(cur_date.toString());

    //set slider value
    int val = this->maximum()*((double)(cur_pts - _main_ui->_begin_pts)/(_main_ui->_end_pts - _main_ui->_begin_pts));
    if(val < 0)
        return ;
    this->setValue(val);



}


player_ui::player_ui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::player_ui)
{
    ui->setupUi(this);

    _config_ui = 0;
    init_pb_icon(ui->pb_play_pause, "./icon/play.png");
    init_pb_icon(ui->pb_open, "./icon/open.png");
    _player_size = PLAY_SIZE;

    for(int index = 0 ;index < _player_size; ++index)
    {
        _play[index] = 0;
        _players[index] = 0;
    }

    new_play = 0;
    new_player = 0;
    _init_play = 0;
    _mode = -1;

    _slider = new player_slider(this);
    QString currentPath = QApplication::applicationDirPath();
    currentPath += "/icon/slider_qss.qss";
    QFile file(currentPath);
    if(file.exists())
    {
        file.open(QIODevice::ReadOnly);
        QByteArray data = file.readAll();
        _slider->setStyleSheet(data.data());
    }

    this->ui->horizontalLayout_pts->addWidget(_slider);
    _slider->setRange(0,1000);
    _slider->hide();
    end_pts = new QLabel("23:59:59",this);
    this->ui->horizontalLayout_pts->addWidget(end_pts);
    end_pts->hide();

    QString icon_path = QApplication::applicationDirPath();
    icon_path += "/icon/timg.ico";
    this->setWindowIcon(QIcon(currentPath));

}

void player_ui::init(int mode)
{
    for(int index = 0;index < _player_size; ++index)
    {
        _players[index] = new player_window(this,index,ui->widget_play);

        //size
        _players[index]->setMinimumSize(300,200);

        //bgcolor
        QPalette pa(_players[index]->palette());
        pa.setColor(QPalette::Background,Qt::black);
        _players[index]->setAutoFillBackground(true);
        _players[index]->setPalette(pa);
        _players[index]->show();   //there must call show()!

    }

    //layout
    if(_players[3])
    {
        ui->horizontalLayout_bottom->addStretch(1);
        ui->horizontalLayout_bottom->addWidget(_players[3]);
        ui->horizontalLayout_bottom->addStretch(1);
    }

    if(_players[0])
    {
        ui->horizontalLayout_middle_1->addStretch(1);
        ui->horizontalLayout_middle_1->addWidget(_players[0]);
        ui->horizontalLayout_middle_1->addStretch(1);
    }

    if(_players[1]||_players[2])
    {
        if(_players[1])
        {
            ui->horizontalLayout_middle_2->addStretch(1);
            ui->horizontalLayout_middle_2->addWidget(_players[1]);
            ui->horizontalLayout_middle_2->addStretch(1);
        }

        ui->horizontalLayout_middle_2->addStretch(1);
        ui->horizontalLayout_middle_2->addStretch(1);
        if(_players[2])
            ui->horizontalLayout_middle_2->addWidget(_players[2]);
        ui->horizontalLayout_middle_2->addStretch(1);

    }

    //new_player
    new_player = new player_window(this,-1,ui->widget_play);

    init_new_player(new_player);
    new_player->hide();

    if(mode!=_mode)
    {
        if(mode)
        {
            for(int index = 0;index < _player_size; ++index)
                _play[index] = new req_play;
            new_play = new req_play;

        }
        else
        {
            for(int index = 0;index < _player_size; ++index)
                _play[index] = new sub_play;
            new_play = new sub_play;
        }
        _mode = mode;
    }

}



player_ui::~player_ui()
{
    delete ui;
}

void player_ui::mouseMoveEvent(QMouseEvent* event)
{

}

void player_ui::deleter()
{

    QLayoutItem *item;
    while(item=ui->horizontalLayout_middle_1->takeAt(0))
    {
        ui->horizontalLayout_middle_1->removeItem(item);
        delete item;
    }
    while(item=ui->horizontalLayout_middle_2->takeAt(0))
    {
        ui->horizontalLayout_middle_2->removeItem(item);
        delete item;
    }
    while(item=ui->horizontalLayout_bottom->takeAt(0))
    {
        ui->horizontalLayout_bottom->removeItem(item);
        delete item;
    }

    while(item=ui->horizontalLayout_top->takeAt(0))
    {
        ui->horizontalLayout_top->removeItem(item);
        delete item;
    }

    for(int index = 0;index < _player_size;++index)
    {
        delete _players[index];
        _players[index] = 0;
    }

    delete new_player;
    new_player = 0;

}

void player_ui::run_play()
{
    init_pb_icon(ui->pb_play_pause, "./icon/pause.png");

    //get args
    int lab = _config_ui->_cur_lab;
    int dev = _config_ui->_cur_dev;
    int mode = _config_ui->_cur_mode;
    QDate date = _config_ui->_cur_date;
    init(mode);
    _slider->show();
    end_pts->show();
    _key_vec.clear();
    gen_pts_range_by_qdate(&date,&_begin_pts,&_end_pts);
    if(!mode) //real-time
    {
        for(int index = 0;index < _player_size; ++index)
        {
            run(_play[index],_players[index],lab,dev,index);
            h264_key key{lab, dev, index};
            _key_vec.push_back(key);
        }
        _slider->setEnabled(false);
    }
    else
    {
        //set slider
        _slider->setEnabled(true);
        for(int index = 0;index < _player_size; ++index)
        {
            run(_play[index],_players[index],lab,dev,index);
            h264_key key{lab, dev, index};
            _key_vec.push_back(key);
        }
    }

}

void player_ui::on_pb_play_pause_clicked()
{
    if(_config_ui == 0)
    {
        _config_ui = new player_config_ui(this);
    }

    if(_init_play)  //pause or play
    {
        if(_players[0])
        {
            if(_play[0]->get_status()&PS_PAUSE)
                init_pb_icon(ui->pb_play_pause, "./icon/pause.png");
            else
                init_pb_icon(ui->pb_play_pause, "./icon/play.png");
            for(int index = 0 ;index < _player_size; ++index)
                _play[index]->play_or_pause();
        }
        else
        {
            if(new_play->get_status()&PS_PAUSE)
                init_pb_icon(ui->pb_play_pause, "./icon/pause.png");
            else
                init_pb_icon(ui->pb_play_pause, "./icon/play.png");
            new_play->play_or_pause();
        }

        return ;

    }

    if(_config_ui->exec() == 0)
    {
        int closed = -1;
        if(player_config_ui::flag !=0)  //ok
        {
            _config_ui->flag = 1;
            deleter();
            run_play();
            _init_play = 1;
        }
    }
}

void player_ui::on_pb_open_clicked()
{
    if(_config_ui == 0)
    {
        _config_ui = new player_config_ui(this);
    }

    if(_config_ui->exec() == 0)
    {
        if(_config_ui->flag == 2)
        {
            if(new_play && !new_player->isHidden())   //full screen
            {
                deleter();
                new_play->term();
                new_player  = new player_window(this,ui->widget_play);
                init_new_player(new_player);
                new_player->show();

                int lab = _config_ui->_cur_lab;
                int dev = _config_ui->_cur_dev;
                int mode = _config_ui->_cur_mode;
                this->_mode = mode;

                if(mode)
                {
                    new_play = new req_play;
                }
                else
                {
                    new_play = new sub_play;
                }
                run(new_play,new_player,lab,dev,0);
            }
            else if(_players[0])   //mini_screen
            {
                for(int index = 0;index < _player_size; ++index)
                    _play[index]->term();
                deleter();

                run_play();
            }
            else   //init
            {
                deleter();
                run_play();
                _init_play = 1;
            }
        }

    }
}
